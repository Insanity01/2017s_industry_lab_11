package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeightButton;
    private JTextField textBMI;
    private JTextField textHealthyWeight;
    private JTextField textHeight;
    private JTextField textWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        this.calculateBMIButton = new JButton("Calculate Healthy BMI");
        this.calculateHealthyWeightButton = new JButton("Calculate Healthy Weight");
        this.textBMI = new JTextField(25);
        this.textHealthyWeight = new JTextField(25);
        this.textHeight = new JTextField(25);
        this.textWeight = new JTextField(25);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel label = new JLabel("Height in Metres");
        JLabel label1 = new JLabel("Weight in Kilos");
        JLabel label2 = new JLabel("Your Body Mass Index is");
        JLabel label3 = new JLabel("Max healthy weight for your height");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)

        this.add(label);
        this.add(textHeight);
        this.add(label1);
        this.add(textWeight);
        this.add(calculateBMIButton);
        this.add(label2);
        this.add(textBMI);
        this.add(calculateHealthyWeightButton);
        this.add(label3);
        this.add(textHealthyWeight);

        // TODO Add Action Listeners for the JButtons
        calculateHealthyWeightButton.addActionListener(this);
        calculateBMIButton.addActionListener(this);
    }

    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        Double BMI;
        Double IdealWeight;
        Double Height = Double.valueOf(textHeight.getText());
        Double Weight = Double.valueOf(textWeight.getText());

        if (event.getSource() == calculateBMIButton) {
            BMI = Weight / (Height * Height);
            textBMI.setText("" + roundTo2DecimalPlaces(BMI));
        }

        if (event.getSource() == calculateHealthyWeightButton) {

            IdealWeight = 24.9 * Height * Height;
            textHealthyWeight.setText("" + roundTo2DecimalPlaces(IdealWeight));
        }


    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}