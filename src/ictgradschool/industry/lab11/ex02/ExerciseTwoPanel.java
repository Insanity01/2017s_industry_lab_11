package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 * <p>
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {
    private JTextField text1;
    private JTextField text2;
    private JTextField textResults;
    private JButton addButton;
    private JButton subtractButton;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        this.text1 = new JTextField(10);
        this.text2 = new JTextField(10);
        this.textResults = new JTextField(20);
        this.addButton = new JButton("Add");
        this.subtractButton = new JButton("Subtract");
        JLabel resultsLabel = new JLabel("Result:");

        this.add(text1);
        this.add(text2);
        this.add(addButton);
        this.add(subtractButton);
        this.add(resultsLabel);
        this.add(textResults);

        addButton.addActionListener(this);
        subtractButton.addActionListener(this);

    }

    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == addButton) {
            double results = Double.valueOf(text1.getText()) + Double.valueOf(text2.getText());
            textResults.setText("" + results);
        }

        if (event.getSource() == subtractButton) {
            double results = Double.valueOf(text1.getText()) - Double.valueOf(text2.getText());
            textResults.setText("" + results);
        }


    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {

        return ((double) Math.round(amount * 100)) / 100;
    }

}